//
//  TabBarVC.swift
//  Photo notes
//
//  Created by Александр on 03/02/2019.
//  Copyright © 2019 Александр Андрюшин. All rights reserved.
//

import UIKit

class TabBarVC: UITabBarController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
       setup()
    }
    
    private func setup() {
        
        let firstViewController = MainVC()
        firstViewController.tabBarItem = UITabBarItem(tabBarSystemItem: .mostViewed, tag: 0)
        
        let secondViewController = SecondVC()
        secondViewController.tabBarItem = UITabBarItem(tabBarSystemItem: .bookmarks, tag: 1)
        
        let thirdViewController = ThirdVC()
        thirdViewController.tabBarItem = UITabBarItem(tabBarSystemItem: .contacts, tag: 2)
        
        let fourthViewController = FourthVC()
        fourthViewController.tabBarItem = UITabBarItem(tabBarSystemItem: .more, tag: 3)
        
        
        let tabBarList = [firstViewController, secondViewController, thirdViewController, fourthViewController]
        viewControllers = tabBarList
        
        
    
}
}
