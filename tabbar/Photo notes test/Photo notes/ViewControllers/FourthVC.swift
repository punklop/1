//
//  FourthVC.swift
//  Photo notes
//
//  Created by Александр on 03/02/2019.
//  Copyright © 2019 Александр Андрюшин. All rights reserved.
//

import UIKit


class FourthVC: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setup()
    }
    
    private func setup() {
        view.backgroundColor = .white
    }
}
