//
//  MainVC.swift
//  Photo notes
//
//  Created by Александр on 03/02/2019.
//  Copyright © 2019 Александр Андрюшин. All rights reserved.
//

import UIKit

class MainVC:  UIViewController, UICollectionViewDataSource, UICollectionViewDelegate {
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    private let kImgName = "image_"
    private let photo = [#imageLiteral(resourceName: "image_10"),#imageLiteral(resourceName: "image_12"),#imageLiteral(resourceName: "image_16"),#imageLiteral(resourceName: "image_6"),#imageLiteral(resourceName: "image_19"),#imageLiteral(resourceName: "image_18"),#imageLiteral(resourceName: "image_1"),#imageLiteral(resourceName: "image_4"),#imageLiteral(resourceName: "image_15"),#imageLiteral(resourceName: "image_10"),#imageLiteral(resourceName: "image_6"),#imageLiteral(resourceName: "image_14"),#imageLiteral(resourceName: "image_3"),#imageLiteral(resourceName: "image_20"),#imageLiteral(resourceName: "image_10"),#imageLiteral(resourceName: "image_16"),#imageLiteral(resourceName: "image_17"),#imageLiteral(resourceName: "image_9"),#imageLiteral(resourceName: "image_4")]

    override func viewDidLoad() {
        super.viewDidLoad()
        
        collectionView.delegate = self
        collectionView.dataSource = self

        self.title = collectionView.collectionViewLayout.description
        
        self.collectionView.contentInset = UIEdgeInsets(top: 8, left: 8, bottom: 8, right: 8)
        collectionView.register(UINib(nibName: "CollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "collectionViewCell")
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return photo.count
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "collectionViewCell", for: indexPath) as! CollectionViewCell
        
        let image = photo[indexPath.item]
        cell.imageView.image = image
        
        
        return cell
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        self.collectionView.collectionViewLayout.invalidateLayout()
    }
}

