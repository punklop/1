//
//  CollectionViewCell.swift
//  Photo notes
//
//  Created by Александр on 03/02/2019.
//  Copyright © 2019 Александр Андрюшин. All rights reserved.
//

import UIKit

class CollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var imageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
         imageView.layer.cornerRadius = 8
         imageView.clipsToBounds = true
    }

}
