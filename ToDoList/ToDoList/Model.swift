//
//  Model.swift
//  ToDoList
//
//  Created by Александр on 01/12/2018.
//  Copyright © 2018 Александр Андрюшин. All rights reserved.
//

import Foundation


var toDoList: [[String: Any]] {
    set {
        UserDefaults.standard.set(newValue, forKey: "toDoDataKey")
        UserDefaults.standard.synchronize()
    }
    
    get {
        if let array = UserDefaults.standard.array(forKey: "toDoDataKey") as? [[String: Any]] {
            return array
        } else {
            return []
        }
    }
}

func addItem(nameItem: String, isCompleted: Bool = false) {
    toDoList.append(["Name": nameItem, "isCompleted": isCompleted])
}


func removeItem(at Index: Int) {
    toDoList.remove(at: Index)
}


func moveItem(fromIndex: Int, toIndex: Int) {
    let from = toDoList[fromIndex]
    toDoList.remove(at: fromIndex)
    toDoList.insert(from, at: toIndex)
}

func changeState(at item: Int) -> Bool {
    toDoList[item]["isCompleted"] = !((toDoList[item]["isCompleted"] as? Bool)!)
    return toDoList[item]["isCompleted"] as! Bool
}


//
//func saveData() {
//    UserDefaults.standard.set(toDoList, forKey: "toDoDataKey")
//    UserDefaults.standard.synchronize()
//}
//
//func loadData() {
//    if let array = UserDefaults.standard.array(forKey: "toDoDataKey") as? [[String: Any]] {
//        toDoList = array
//    } else {
//        toDoList = []
//    }

