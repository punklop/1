//
//  ViewController.swift
//  UISegmentControll
//
//  Created by Александр on 03/12/2019.
//  Copyright © 2019 Александр Андрюшин. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    var uiElements = ["UISegmentControll",
                      "UILabel",
                      "UISlider",
                      "UITextField",
                      "UIButton",
                      "UIDatePicker",
                      "UISwitch"]
    
    
    var selectedElements: String?
    
    @IBOutlet weak var segmentedConrol: UISegmentedControl!
    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var slider: UISlider!
    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var buttonOutlet: UIButton!
    @IBOutlet weak var datePicker: UIDatePicker!
    @IBOutlet weak var switchLabel: UILabel!
    @IBOutlet weak var switchOutlet: UISwitch!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setup()
    }
    
    func setup() {
        
        label.text = String(slider.value)
        //label.isHidden = true
        label.font = label.font.withSize(35)
        
        segmentedConrol.insertSegment(withTitle: "Third", at: 2, animated: true)
        
        //slider.value = 0.7
        slider.minimumValue = Float(0)
        slider.maximumValue = Float(1)
        slider.minimumTrackTintColor = .black
        slider.maximumTrackTintColor = .yellow
        slider.thumbTintColor = .red
        
        buttonOutlet.layer.cornerRadius = 50
        
        datePicker.locale = Locale(identifier: "ru_Ru")
        
        choiceUiElement()
        createToolBar()
    }
    
    func hideAllElements() {
        
        segmentedConrol.isHidden = true
        slider.isHidden = true
        label.isHidden = true
        buttonOutlet.isHidden = true
        datePicker.isHidden = true
        switchOutlet.isHidden = true
        switchLabel.isHidden = true
    }
    
    func choiceUiElement() {
    
    let elementPicker = UIPickerView()
    elementPicker.delegate = self
    
    textField.inputView = elementPicker
        
        elementPicker.backgroundColor = .brown
        
    }
    
    func createToolBar() {
        
        let toolbar = UIToolbar()
        toolbar.sizeToFit()
        
        let doneButton = UIBarButtonItem(title: "Done",
                                         style: .plain,
                                         target: self,
                                         action: #selector(dismisKeyboard))
        
        toolbar.setItems([doneButton], animated: true)
        toolbar.isUserInteractionEnabled = true
        
        textField.inputAccessoryView = toolbar
        
        toolbar.tintColor = .white
        toolbar.barTintColor = .brown
    }
    
    @objc func dismisKeyboard() {
        view.endEditing(true)
    }

    @IBAction func choiceSemgent(_ sender: UISegmentedControl) {
        
        label.isHidden = false
        
        switch segmentedConrol.selectedSegmentIndex {
        case 0:
            label.text = "The first semgent is selected"
            label.textColor = .red
            label.font = label.font.withSize(20)
        case 1:
            label.text = "The second segment is selected"
            label.textColor = .blue
            label.font = label.font.withSize(30)
        case 2:
            label.text = "The third segment is selected"
            label.textColor = .gray
            label.font = label.font.withSize(40)
        default:
            print("Error")
        }
    }
    
    @IBAction func sliderAction(_ sender: UISlider) {
        label.text = String(sender.value)
        
        let backGroundColor = self.view.backgroundColor
        self.view.backgroundColor = backGroundColor?.withAlphaComponent(CGFloat(sender.value))
    }
    
    @IBAction func tapButton(_ sender: UIButton) {
        
        guard textField.text?.isEmpty == false else { return }
        
        if let _ = Double(textField.text!) {
            
            let alert = UIAlertController(title: "Не верный формат", message: "Имя не может содержать цифры", preferredStyle: .alert)
            let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
            alert.addAction(okAction)
            present(alert, animated: true, completion: nil)
            
        } else {
            label.text = textField.text
            textField.text = nil
        }
    }
    
    @IBAction func datePickerAction(_ sender: UIDatePicker) {
        
        let dateFormater = DateFormatter()
        dateFormater.dateStyle = .full
        dateFormater.locale = Locale(identifier: "ru_RU")
        
        let dateValue = dateFormater.string(from: sender.date)
        label.text = dateValue
    }
    
    @IBAction func switchAction(_ sender: UISwitch) {
        segmentedConrol.isHidden = !segmentedConrol.isHidden
        label.isHidden = !label.isHidden
        textField.isHidden = !textField.isHidden
        slider.isHidden = !slider.isHidden
        buttonOutlet.isHidden = !buttonOutlet.isHidden
        datePicker.isHidden = !datePicker.isHidden
        
        if sender.isOn {
            switchLabel.text = "Скрыть все элементы"
        } else {
            switchLabel.text = "Показать все элементы"
        }
    }
}

extension ViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }

    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return uiElements[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return uiElements.count
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        selectedElements = uiElements[row]
        textField.text = selectedElements
        
        switch row {
        case 0:
            hideAllElements()
            segmentedConrol.isHidden = false
        case 1:
            hideAllElements()
            label.isHidden = false
            switchLabel.isHidden = false
        case 2:
            hideAllElements()
            slider.isHidden = false
        case 3:
            hideAllElements()
        case 4:
            hideAllElements()
            buttonOutlet.isHidden = false
        case 5:
            hideAllElements()
            datePicker.isHidden = false
        case 6:
            hideAllElements()
            switchOutlet.isHidden = false
        default:
            hideAllElements()
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        
        var pickerViewLabel = UILabel()
        
        if let currentLabel = view as? UILabel {
            pickerViewLabel = currentLabel
        } else {
            pickerViewLabel = UILabel()
        }
        
        pickerViewLabel.textColor = .white
        pickerViewLabel.textAlignment = .center
        pickerViewLabel.font = UIFont(name: "Noteworthy", size: 27)
        pickerViewLabel.text = uiElements[row]
        
        return pickerViewLabel
    }
}
